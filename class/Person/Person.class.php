<?php 
include_once('Person_A.class.php');
require_once(realpath($_SERVER['DOCUMENT_ROOT'] . '\\busesParaTodos\\dirs.php'));
include_once(realpath(TRAITS . 'functions.trait.php'));
/**
 * 
 */
class Person
{	
	use functionsTrait;

	protected $name;
	protected $lastName;
	protected $email;
	protected $dni;
	protected $table_name = 'person';
	

	protected function setName($value)
	{
		$this->name = $value;
	}

	public function getName()
	{
		return $this->name;
	}

	protected function setLastName($value)
	{
		$this->lastName = $value;
	}

	public function getLastName()
	{
		return $this->lastName;
	}

	protected function setEmail($value)
	{
		$this->email = $value;
	}

	public function getEmail()
	{
		return $this->email;
	}

	protected function setDni($value)
	{
		$this->dni = $value;
	}

	public function getDni()
	{
		return $this->dni;
	}

	public function fullName(){
		return "{$this->name} {$this->lastName}";
	}

	public function getTableName()
	{
		return $this->table_name;
	}


	public function checkArr($array)
	{
		if (!is_array($array)) {
			throw new Exception("The element isn't an array");
		}

		if (!isset($array['name_modal'])) {
			throw new Exception("The field name_modal is empty o null");
			
		}
		if (!isset($array['lastName_modal'])) {
			throw new Exception("The field lastName_modal is empty o null");
			
		}
		if (!isset($array['dni_modal'])) {
			throw new Exception("The field dni_modal is empty o null");
			
		}

		if (array_key_exists('email_modal', $array)) {
			if (!isset($array['email_modal'])) {
				throw new Exception("The field email_modal is empty o null");
			}
		}
		
		return $array;
	}


	public function createAllSetters($array)
	{
		if (!is_array($array)) {
			throw new Exception("The element isn't an array");
		}

		if (!$this->checkArr($array)) {
			return;
		}
		
		if ($array['name_modal'] == "") {
			throw new Exception("The value name_modal is empty");
			
		}

		if ($array['lastName_modal'] == "") {
			throw new Exception("The value lastName_modal is empty");
			
		}

		if ($array['dni_modal'] == "" || !$this->validationDNI($array['dni_modal'])) {
			throw new Exception("The value dni_modal is empty or incorrect format");
		}

		if (array_key_exists('email_modal', $array)) {

			// print_r(filter_var(filter_var($array['email_modal'],FILTER_SANITIZE_EMAIL),FILTER_VALIDATE_EMAIL)); die;
			if (!filter_var(filter_var($array['email_modal'],FILTER_SANITIZE_EMAIL),FILTER_VALIDATE_EMAIL)) {
				throw new Exception("This value email_modal isn't email");
				
			}
			$this->setEmail(filter_var($array['email_modal'], FILTER_SANITIZE_EMAIL));
		}

		$this->setName(ucfirst(ltrim(rtrim($array['name_modal']))));
		$this->setLastName(ucfirst(ltrim(rtrim($array['lastName_modal']))));
		$this->setDni(ltrim(rtrim($array['dni_modal'])));
		
	}

}

 ?>