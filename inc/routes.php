<?php  

use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/******************Ruta Index Buses Para Todos**********************************************/
$route = new Route('/busesParaTodos/index.php');
$routes = new RouteCollection();
$routes->add('bus_index', $route);
$context = new RequestContext();

$generator = new UrlGenerator($routes, $context);
$url = $generator->generate('bus_index');

/****************************************************************/


/***********************Ruta Index Service*****************************************/
// ./controllers/Services.controller/services.controller.php?action=index
$route_2 = new Route('/busesParaTodos/controllers/Services.controller/services.controller.php');
$routes->add('servicio_index', $route_2);
$generator = new UrlGenerator($routes, $context);
$url_2 = $generator->generate('servicio_index',
							 ['action' => '[\w+]',
							  'view' =>	  '[\w+]',
							 ]);

/*****************************************************************/

/**************************Ruta New Service***************************************/
// "./services.controller.php?action=show"
$route_3 = new Route('/busesParaTodos/controllers/Services.controller/services.controller.php');
$routes->add('services_show', $route_2);
$generator = new UrlGenerator($routes, $context);
$url_3 = $generator->generate('services_show',
							 ['action' => '[\w+]',
							  'view' =>	  '[\w+]',
							 ]);

/*****************************************************************/



/***************Route Delete Service**************************************************/
$route_4 = new Route('/busesParaTodos/controllers/Services.controller/services.controller.php');
$routes->add('services_delete', $route_4);
$generator = new UrlGenerator($routes, $context);
$url_4 = $generator->generate('services_delete',
							 ['action' => '[\w+]',
							  'view' =>	  '[\w+]',
							  'id' => '[\d+]'
							 ]);

/*****************************************************************/


/***************Route Delete Service**************************************************/
$route_5 = new Route('/busesParaTodos/controllers/Services.controller/services.controller.php');
$routes->add('services_update', $route_5);
$generator = new UrlGenerator($routes, $context);
$url_5 = $generator->generate('services_update',
							 ['action' => '[\w+]',
							  'view' =>	  '[\w+]',
							  'id' => '[\d+]'
							 ]);

/*****************************************************************/