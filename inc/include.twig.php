<?php 
// require_once(realpath($_SERVER['DOCUMENT_ROOT'] . '\\busesParaTodos\\dirs.php'));
// require_once(INC . '\\'); ->routes.php

/*Asi como esta funca, hay que acomodar las clases*/
// $twig->addExtension(
// 	new \Symfony\Bridge\Twig\Extension\AssetExtension(
// 		new \Symfony\Component\Asset\Packages(
// 			new \Symfony\Component\Asset\PathPackage(
// 				'busesParaTodos/public/',
// 				new \Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy()
				
// 			)
// 		)
// 	)
// );
/*Funca*/

namespace Symfony\Bridge\Twig\Extension;

use Symfony\Component\Asset\Packages;
use Symfony\Component\Asset\PathPackage;
// use Symfony\Component\Asset\VersionStrategy;



/***************************Probando****************************************/
/*Clases para la creacion de rutas*/

use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;


/****************************************************************************/

require_once(realpath($_SERVER['DOCUMENT_ROOT'] . '\\busesParaTodos\\dirs.php'));
require_once(realpath(ROOT_PATH . 'config.php'));

// require_once('../../dirs.php');
require_once realpath(ROOT_PATH . '\\vendor\\autoload.php');

// echo realpath(ROOT_PATH . '\\vendor\\autoload.php'); die;
// echo realpath(ROOT_PATH . '\\views'); die; 


// date_default_timezone_set(TIME_ZONE);


$loader = new \Twig\Loader\FilesystemLoader(
	[
		realpath(ROOT_PATH . '\\views'),
		realpath(ROOT_PATH . '\\views\\services'),
		realpath(ROOT_PATH . '\\views\\bus'),
	]
);

$twig = new \Twig\Environment($loader, [
	'cache' => realpath(ROOT_PATH . '\\cache\\twig') , 'debug' => true,
]);



$assetExtension = new AssetExtension(
					new Packages(
						new PathPackage(
							'busesParaTodos/public/',
							new \Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy()
						)
					)
				);





/******************Ruta Index Buses Para Todos**********************************************/
$route = new Route('/busesParaTodos/index.php');
$routes = new RouteCollection();
$routes->add('bus_index', $route);
$context = new RequestContext();

$generator = new UrlGenerator($routes, $context);
$url = $generator->generate('bus_index');

/****************************************************************/


/***********************Ruta Index Service*****************************************/
// ./controllers/Services.controller/services.controller.php?action=index
$route_2 = new Route('/busesParaTodos/controllers/Services.controller/services.controller.php');
$routes->add('servicio_index', $route_2);
$generator = new UrlGenerator($routes, $context);
$url_2 = $generator->generate('servicio_index',
							 [
							  'page' => 1,
							  'action' => '[\w+]',
							  'view' =>	  '[\w+]',
							 ]);

/*****************************************************************/

/**************************Ruta New Service***************************************/
// "./services.controller.php?action=show"
$route_3 = new Route('/busesParaTodos/controllers/Services.controller/services.controller.php');
$routes->add('services_show', $route_2);
$generator = new UrlGenerator($routes, $context);
$url_3 = $generator->generate('services_show',
							 [
							   'page' => '[\d+]',	
							  'action' => '[\w+]',
							  'view' =>	  '[\w+]',
							 ]);

/*****************************************************************/



/***************Route Delete Service**************************************************/
$route_4 = new Route('/busesParaTodos/controllers/Services.controller/services.controller.php');
$routes->add('services_delete', $route_4);
$generator = new UrlGenerator($routes, $context);
$url_4 = $generator->generate('services_delete',
							 ['action' => '[\w+]',
							  'view' =>	  '[\w+]',
							  'id' => '[\d+]'
							 ]);

/*****************************************************************/


/***************Route Delete Service**************************************************/
$route_5 = new Route('/busesParaTodos/controllers/Services.controller/services.controller.php');
$routes->add('services_update', $route_5);
$generator = new UrlGenerator($routes, $context);
$url_5 = $generator->generate('services_update',
							 ['action' => '[\w+]',
							  'view' =>	  '[\w+]',
							  'id' => '[\d+]'
							 ]);

/*****************************************************************/








$routingExtension = new \Symfony\Bridge\Twig\Extension\RoutingExtension(
	new \Symfony\Component\Routing\Generator\UrlGenerator(
		$routes, 
		 new \Symfony\Component\Routing\RequestContext()
	),
);

$twig->addExtension(new \Twig\Extension\DebugExtension());
$twig->addExtension($assetExtension);
$twig->addExtension($routingExtension);
$twig->getExtension(\Twig\Extension\CoreExtension::class)->setTimezone(TIME_ZONE);







?>