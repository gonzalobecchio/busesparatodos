<?php 
require_once(realpath($_SERVER['DOCUMENT_ROOT'] . '\\busesParaTodos\\dirs.php'));
require_once(CLASS_PATH . 'Passenger\\passenger.class.php');

$passenger = new Passenger();

switch ($_REQUEST['action']) {
	case 'verifyPassenger':
		$result = $passenger->verifyPassenger($_POST['dni']);
		echo json_encode($result);
	break;
	case 'getAllPassenger':
		$result = $passenger->getAllPassenger($_POST['dni']);
		echo json_encode($result);
	break;
	case 'setDatesNewPassenger':
		$result = $passenger->setDatesNewPassenger($_POST);
		echo json_encode($result);
	break;
	
	default:
		echo "This action not found";
	break;
}