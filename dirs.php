<?php 

/*DIRECTORIO DEL PROYECTO*/
define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT'] . 'busesParaTodos'); //Directorio del proyecto

/*DIRECTORIO DEL ARCHIVO*/
define('ROOT_PATH', __DIR__ . '\\'); //Path root de archivo que invoca

/*Rutas controllers*/
//Rutas de controlador
define('CONTROLLER_PATH', ROOT_PATH . '\\controllers'); //Path root Controllers

 /*Controllers/Services.controlles/services.controller.php*/
define('SERVICES_CONTROLLER', CONTROLLER_PATH . '\\Services.controller\\services.controller.php');



define('INC', ROOT_PATH . '\\inc'); //Path route inc

/*Ruta views*/

define('VIEWS_PATH', ROOT_PATH . 'views');


//Definiendo ruta de acceso a clases
define('CLASS_PATH', ROOT_PATH . 'class' .'\\' );

define('TRAITS', ROOT_PATH . 'traits\\');


// echo ROOT_PATH . 'config.php';	
 

 ?>