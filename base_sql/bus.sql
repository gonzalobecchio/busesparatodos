/*
Navicat MariaDB Data Transfer

Source Server         : databases
Source Server Version : 100424
Source Host           : localhost:3306
Source Database       : bus

Target Server Type    : MariaDB
Target Server Version : 100424
File Encoding         : 65001

Date: 2022-05-28 14:52:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for availability
-- ----------------------------
DROP TABLE IF EXISTS `availability`;
CREATE TABLE `availability` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_services` int(10) DEFAULT NULL,
  `availability_day` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for bus
-- ----------------------------
DROP TABLE IF EXISTS `bus`;
CREATE TABLE `bus` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `bus_model` varchar(255) DEFAULT '',
  `bus_seats` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for person
-- ----------------------------
DROP TABLE IF EXISTS `person`;
CREATE TABLE `person` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `person_name` varchar(255) DEFAULT NULL,
  `person_lastname` varchar(255) DEFAULT NULL,
  `person_email` varchar(255) DEFAULT NULL,
  `person_dni` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for place
-- ----------------------------
DROP TABLE IF EXISTS `place`;
CREATE TABLE `place` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `place_name` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `place_name` (`place_name`),
  KEY `place_name_2` (`place_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for services
-- ----------------------------
DROP TABLE IF EXISTS `services`;
CREATE TABLE `services` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_o` int(10) DEFAULT NULL,
  `id_d` int(10) DEFAULT NULL,
  `id_bus` int(10) DEFAULT NULL,
  `services_hs` time DEFAULT NULL,
  `services_hll` time DEFAULT NULL,
  `service_price` float(10,2) DEFAULT NULL,
  `service_estate` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_o` (`id_o`),
  KEY `id_d` (`id_d`),
  CONSTRAINT `id_d` FOREIGN KEY (`id_d`) REFERENCES `place` (`id`),
  CONSTRAINT `id_o` FOREIGN KEY (`id_o`) REFERENCES `place` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for services_details
-- ----------------------------
DROP TABLE IF EXISTS `services_details`;
CREATE TABLE `services_details` (
  `id` int(10) NOT NULL,
  `id_services` int(10) DEFAULT NULL,
  `id_passenger` int(10) DEFAULT NULL,
  `services_date` datetime DEFAULT NULL,
  `services_seat` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
