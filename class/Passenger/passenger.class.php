<?php 
require_once(realpath($_SERVER['DOCUMENT_ROOT'] . '\\busesParaTodos\\dirs.php'));
require_once('../../config.php');
// require_once(ROOT_PATH . "config.php"); 
include_once(CLASS_PATH . 'bd.php');
include_once(CLASS_PATH . 'Person\\Person.class.php');

/**
 * 
 */
class Passenger extends Person
{
	public $db;

	function __construct()
	{
		$this->db = new BD();
	}


	public function verifyPassenger($value)
	{
		$this->db->query = "SELECT COUNT(person.id) as id_persona FROM {$this->getTableName()} WHERE person.person_dni = {$value}";
		$result = $this->db->ejecQuery($this->db->query);
		return ($result[0]['id_persona']);
	}

	public function getAllPassenger($value)
	{
		$this->db->query ="SELECT id,person_name,person_lastname,person_email,person_dni FROM {$this->getTableName()} WHERE person.person_dni = {$value}";
		$result = $this->db->ejecQuery($this->db->query);
		return $result;

	}

	public function setDatesNewPassenger($array)
	{
		try {
			$this->createAllSetters($array);
			$this->db->query = "INSERT INTO {$this->getTableName()} (person_name,person_lastname,person_email,person_dni) VALUES ('{$this->getName()}','{$this->getLastName()}','{$this->getEmail()}','{$this->getDni()}')";
			// echo $this->db->query; die;
			return $this->db->ejecQuery($this->db->query);

		} catch (Exception $e) {
			echo $e->getMessage();
		}

		
	}
}
