<?php 
require_once(realpath($_SERVER['DOCUMENT_ROOT'] . '\\busesParaTodos\\dirs.php'));


require_once(realpath(CLASS_PATH . 'bd.php'));

/**
 * 
 */
class Place
{
	private $id;
	private $name;
	private $db;
	public $query;
	private $table = "PLACE"; 

	function __construct()
	{
		$this->db = new BD();
	}

	public function getTableName()
	{
		return $this->table;
	}

	public function getAllPlace()
	{
		$this->query = "SELECT * FROM {$this->getTableName()} ORDER BY place_name DESC";
		$this->db->ejecQuery($this->query);
		if (!$this->db->ejecQuery($this->query)) {
			return false;
		}
		// echo '<pre>';
		// echo json_encode($this->db->ejecQuery($this->query));
		// echo '</pre>'; die;
		return json_encode($this->db->ejecQuery($this->query));
	}

	
}

 ?>