<?php  

require_once(realpath($_SERVER['DOCUMENT_ROOT'] . '\\busesParaTodos\\dirs.php'));
require_once(CLASS_PATH . 'ServicesDetails/services.details.class.php');
require_once(realpath(ROOT_DIR . '\\config.php'));

$services_details = new ServicesDetails();

switch ($_REQUEST['action']) {
	case 'insertServiceDetails':
		$request_body = file_get_contents('php://input');
		$dates_services = json_decode($request_body);
		$id_insert = $services_details->insertServiceDetails($dates_services[0]);
		echo json_encode($id_insert);
		break;
	
	default:
		echo 'Ha ocurrido un error en controller services_details.controller: ' . __DIR__;
		break;
}