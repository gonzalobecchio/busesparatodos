<?php 
trait functionsTrait {

	public function timeEqual(DateTime $date1, DateTime $date2)
	{
		if ($date2 < $date1) {
			return false;
		}
		if ($date1 == $date2) {
			return false;
		}
		return true;
	}

	/*Recibe un string con fomato fecha - lo divide por año, mes y dia, 
	luego chequea si corresponde a un fecha del calendario*/
	public function is_date($date_string)
	{
		$array_date = explode("-", $date_string);
		list($year, $month,$day) = $array_date;
		return checkdate($month, $day, $year);
	}

	public function validationDNI($value)
	{
		$min = 7;
		$max = 8;
		return strlen($value) > 7 && strlen($value) <= 8 ? true : false;
	}

	

}




?>