<?php 
/**
 * 
 */
abstract class Person_a
{
	abstract protected function setName($value);
	abstract public function getName();
	abstract protected function setLastName($value);
	abstract public function getLastName();
	abstract protected function setEmail($value);
	abstract public function getEmail();
	abstract protected function setDni($value);
	abstract public function getDni();
	abstract public function fullName();

	abstract protected function validate($array);
	

}



?>