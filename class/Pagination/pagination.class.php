<?php  
require_once(realpath($_SERVER['DOCUMENT_ROOT'] . '\\busesParaTodos\\dirs.php'));
include_once(realpath(CLASS_PATH .'bd.php'));
include_once(realpath(ROOT_PATH . 'config.php'));


/**
 * 
 */
class Pagination
{
	private $count_registers;
	/*Cantidad de paginas que posee el paginador*/
	private $pages;
	/*Pagina actual quue se esta mostrando*/
	private $actual_page;
	private $init_pagination;
	public  $query;
	public $bd;

	
	function __construct()
	{
		$this->bd = new BD();
		$this->actual_page = 0;
		$this->pages = 0;
		$this->count_registers = 0;
		$this->init_pagination = 0;

	}



	public function getCountRegisters()
	{
		return $this->count_registers;
	}


	/*Setea con la cantidad de registros que se encuentran en la tabla pasada como parametros*/
	public function setCountRegisters($table)
	{
		$query = "SELECT COUNT(*) as total_rows FROM {$table}";
		$this->count_registers = $this->bd->ejecQuery($query);
		/*Determina la cantidad de paginas a mostrar resultados*/
		$this->pages = (int) ceil($this->count_registers[0]['total_rows'] / ITEM_BY_PAGE);
		return (int) $this->count_registers[0]['total_rows'];
	}

	public function getPages()
	{
		return $this->pages;
	}



	public function getActualPage()
	{
		return $this->actual_page;
	}


	public function setActualPage($actual_page)
	{
		$this->actual_page = $actual_page;

		return $this;
	}

	public function getInitPagination()
	{
		return $this->init_pagination;
	}


	public function setInitPagination($init_pagination)
	{
		$this->init_pagination = $init_pagination;

		return $this;
	}

	/*Encargada de setar los valores de paginacion */
	public function paginator($request)
	{
		/*Los registos comienzan desde cero*/
		if (isset($request) && $request > 1) {
			if ($request > $this->getPages()) {
				$this->setActualPage(1);
				$this->setInitPagination(ITEM_BY_PAGE * 0);
				return;
			}
			$this->setActualPage($request);
			$this->setInitPagination(ITEM_BY_PAGE * ($this->getActualPage() - 1));
		}else{
			if ((isset($request) && $request == 1) || !isset($request) || $request == 0 || $request < 0) {
				$this->setActualPage(1);
				$this->setInitPagination(ITEM_BY_PAGE * 0);
			}
		}
		
	}
}