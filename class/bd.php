<?php 


/**
 * 
 */
class BD
{
	private $host;
	private $user;
	private $password;
	private $bd_name;
	private $mysqli;
	public $error;
	public $query;
	
	function __construct()
	{	
		$this->host = HOST;
		$this->user = USER;
		$this->password = PASSWORD;
		$this->bd_name = BD_NAME;		

		$this->mysqli = new mysqli($this->host, $this->user, $this->password, $this->bd_name);
		if ($this->mysqli->connect_errno) {
			throw new Exception("Error al iniciar la conexion " . $this->mysqli->connect_error);
		}		
	}



	public function close()
	{
		$this->error = $this->mysqli->close();
		if (!$this->error) {
			throw new Exception("Error en la conexion " . $this->mysqli->connect_error);		
		}
		
	}


	// Manejo mysql
	public function ejecQuery($query)
	{
		$string = substr(strtolower(ltrim($query)), 0, 6);

		switch ($string) {
			case 'select':
			$result = $this->mysqli->query(ltrim($query));
			// print_r($result->num_rows); die;
			if ($result->field_count == 0 || $result->num_rows == 0) {
				return array();
			}else{
				while ($fila = $result->fetch_assoc()) {
					$array_datos[] = $fila;
				}
				return $array_datos;
			}
			break;
			case 'insert':
			$result = $this->mysqli->query($query);
			if (!$result) {
				throw new Exception("Error Mysqli: " . $this->mysqli->error);
			}
			return $this->mysqli->insert_id;
			break;
			case 'create':
			$result = $this->mysqli->query($query);
			if (!$result) {
				throw new Exception("Error Mysqli: " . $this->mysqli->error);
			}
			break;
			default:
				throw new Exception("Error Mysqli: " . $this->mysqli->error);
			break;
		}


		

		
	}


}






?>