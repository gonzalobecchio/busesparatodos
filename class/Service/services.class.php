<?php 
require_once(realpath($_SERVER['DOCUMENT_ROOT'] . '\\busesParaTodos\\dirs.php'));
include_once(realpath(CLASS_PATH .'bd.php'));
include_once(realpath(CLASS_PATH .'Pagination/pagination.class.php'));
include_once(realpath(TRAITS . 'functions.trait.php'));
include_once(realpath(ROOT_PATH . 'config.php'));
/**
 * 
 */
class Service
{
	use functionsTrait;

	private $estado;
	private $hs;
	private $hll;
	private $price;
	private $origen;
	private $destino;
	private $bus;
	public 	$db;
	private $result;
	private $table_name = 'services';
	public  $arrayFK = [
						'id_o',
						'id_d',
						'id_bus',
					  ];

	public $pagination;



	function __construct()
	{
		$this->db = new BD();
		$this->pagination = new Pagination();

	}

	
	/*******Setters*******/
	public function setOrigen($value)
	{
		$this->origen = $value;
	}

	public function setDestino($value)
	{
		$this->destino = $value;
	}

	public function setPrice($value)
	{
		$this->price = $value;
	}

	public function setHll($value)
	{
		$this->hll = $value;
	}

	public function setHs($value)
	{
		$this->hs = $value;
	}

	public function setBus($value)
	{
		$this->bus = $value;
	}

	public function setEstado($value)
	{
		$this->estado = $value;
	}


	/*******Getters*******/
	public function getTableName()
	{
		return $this->table_name;
	}

	public function getOrigen()
	{
		return $this->origen;
	}

	public function getDestino()
	{
		return $this->destino;
	}

	public function getPrice()
	{
		return 	$this->price;
	}

	public function getHll()
	{
		return ($this->hll);
	}

	public function getHs()
	{
		return ($this->hs);
	}

	public function getBus()
	{
		return $this->bus;
	}

	public function getEstado()
	{
		return $this->estado;
	}

	
	/******Funcionalidades******/
	/*Return array crudo con datos paginados*/
	public function getAllServices($init_service)
	{
		if (!$init_service) {
			$this->db->query = "SELECT * FROM {$this->getTableName()} LIMIT " . ITEM_BY_PAGE;
		}
		$this->db->query = "SELECT * FROM {$this->getTableName()} ORDER BY id_o ASC LIMIT  {$init_service} ," . ITEM_BY_PAGE;	
		if (!$this->db->ejecQuery($this->db->query)) {
			$this->db->ejecQuery($this->db->query);
		}else{
			return $result = $this->db->ejecQuery($this->db->query);
		}
	}

		// 	echo '<pre>';
		// print_r($result);die;
		// echo '</pre>';
		// 	uasort($result, function($a,$b){
		// 		if ($a == $b) {
		// 			return 0;
		// 		}
		// 		return ($a < $b) ? -1 : 1;
		// 	});
		// }
	// 	echo '<pre>';
	// 	print_r($result);die;
	// 	echo '</pre>';
	// }


	public function getAllServiceOfId($value)
	{
		$this->db->query = "SELECT id,id_o,id_d,id_bus,service_estate FROM {$this->getTableName()} WHERE id_o = '{$value}' GROUP BY id_d";
		if (!$this->db->ejecQuery($this->db->query)) {
			$this->db->ejecQuery($this->db->query);
		}else{
			return $result = $this->db->ejecQuery($this->db->query);
		}
	}


	public function returnEstateStr($value)
	{
		if (is_numeric($value)) {
			if ($value == 1) {
				return 'Disponible';
			}

			if ($value == 0) {
				return 'Completo';
			}
			
		}		
	}


	/*Function recibe fitro=id; campo; y tabla; para devolver el campo place_name*/
	public function idFKtoStr($FK,$field,$table)
	{
		$this->db->query = "select {$field} from {$table} where id = {$FK}";
		$result = $this->db->ejecQuery($this->db->query);
		return $result[0][$field];
	}

	
	
	/*Recibe el value*/
	public function updateRegister(Array $array)
	{
		$base = $array;
		$id_o_int = $array['id_o'];
		$id_d_int = $array['id_d'];
		$id_bus_int = $array['id_bus'];
		$service_estate = $array['service_estate'];

		$reemplazo = [
			'id_o' => $this->idFKtoStr($array['id_o'],'place_name','place') , 
			'id_d' => $this->idFKtoStr($array['id_d'],'place_name','place') ,
			'id_bus' => $this->idFKtoStr($array['id_bus'],'bus_model','bus'),
			'service_estate' => $this->returnEstateStr($service_estate),
			'id_o_int' => $array['id_o'],
			'id_d_int' => $array['id_d'],
			'id_bus_int' => $array['id_bus'],
			'service_estate_int' => $array['service_estate'],
		];
		$register_update = array_replace($base, $reemplazo);
	// echo '<pre>';
	// print_r($register_update);
	// echo '</pre>';
	// die;
		return $register_update;
	}






	/*Function recibe un array con datos crudos de base y devuelve array para presentar en el template*/
	/*Verificar cuando tipamos el paramtro*/
	public function getAllTlp($array)
	{

		$array_new = array();

		if (!isset($array)) {
			return $array_new;
		}else{
			foreach ($array as $key => $value) {
				array_push($array_new, $this->updateRegister($value));
			}
			// echo '<pre>';
			// print_r($array_new);die;
			// echo '</pre>';
			return $array_new;	
		}

	}


	/*Devuelve Array si llegan los valores*/
	/*Verifica el tipo de los campos* */
	/*Al comporbar el tipo time correcto - trasnformarlo a string para la base*/
	public function checkArr($array)
	{
		if (!is_array($array)) {
			return false;
		}
		if (!count($array)) {
			return false;
		}

		if (!isset($array['origen_newService'])) {
			throw new Exception("The field 'origen' is empty");

		}
		if (!isset($array['destino_newService'])) {
			throw new Exception("The field 'destino' is empty");

		}
		if (!isset($array['service_hs'])) {
			throw new Exception("The field 'service_hs' is empty");

		}
		if (!isset($array['service_hll'])) {
			throw new Exception("The field 'origen' is empty");

		}
		if (!isset($array['precio'])) {
			throw new Exception("The field 'origen' is empty");

		}
		if (!isset($array['bus'])) {
			throw new Exception("The field 'bus' is empty");

		}
		return $array;
	}

	public function createAllSetters($array)
	{
		if (!$this->checkArr($array)) {
			return false;
		}
		if (!is_numeric($array['origen_newService'])) {
			throw new Exception("The date 'origen' is not numeric");

		}
		if (!is_numeric($array['destino_newService'])) {
			throw new Exception("The date 'destino' is not numeric");
		}
		if (!($this->timeEqual(new DateTime($array['service_hs']), new DateTime($array['service_hll'])))) {
			throw new Exception("The time are not corrects");

		}	
		if (!is_numeric($array['precio'])) {
			throw new Exception("The date 'precio' is not numeric");
		}
		if (!is_numeric($array['bus'])) {
			throw new Exception("The date 'bus' is not numeric");
		}

		$this->setOrigen($array['origen_newService']);
		$this->setDestino($array['destino_newService']);
		$this->setHs($array['service_hs']);
		$this->setHll($array['service_hll']);
		$this->setPrice($array['precio']);
		$this->setBus($array['bus']);
		$this->setEstado(1);
	}

	public function createService(Array $request)
	{	
		try {
			$this->createAllSetters($request);
			$this->db->query = "insert into {$this->getTableName()} (id_o, id_d, id_bus, services_hs, services_hll, service_price, service_estate) values ('{$this->getOrigen()}','{$this->getDestino()}','{$this->getBus()}','{$this->getHs()}','{$this->getHll()}','{$this->getPrice()}','{$this->getEstado()}')";
		// print_r($this->db->ejecQuery($this->db->query));
			return $this->db->ejecQuery($this->db->query);
		// die;
		} catch (Exception $e) {
			echo $e->getMessage();
		} finally { 
		// header('Location: ?action=index');
		}

	}





	/*Realizar control de datos*/
	public function searchService(Array $request)
	{

		$fecha_form = new DateTime($request['date_buscador']);
		$day_of_week = getdate($fecha_form->getTimestamp());

		if (count($request)) {
			$this->db->query = "SELECT services.id as ID_SERVICIO, services.id_o , services.id_d , services.id_bus , services.services_hs as HORA_SALIDA, services.services_hll as HORA_LLEGADA, services.service_price as SERVICE_PRECIO, availability.availability_day as ID_DAY_AVAILABILITY, services.service_estate FROM services INNER JOIN availability ON services.id = availability.id_services WHERE services.id_o = '{$request['id_o']}' AND services.id_d = '{$request['id_d']}' AND availability.availability_day = '{$day_of_week['wday']}'";
			// echo $this->db->query;
			$result = $this->db->ejecQuery($this->db->query);
			return $result;

		}

	}



	/*Retorna los datos de servicio y datos de la unidad para dibujarla*/
	public function getDatesOfServiceSelected($id_service)
	{
		if (isset($id_service) && is_numeric($id_service)) {
			$this->db->query = "SELECT services.id,services.id_o, services.id_d , services_hs as HORA_SALIDA, services_hll as HORA_LLEGADA, services.service_price as PRECIO, services.service_estate, services.id_bus , bus.bus_model as MODEL , bus.bus_seats FROM bus INNER JOIN services ON bus.id = services.id_bus WHERE services.id = '{$id_service}'";
			$result  = $this->db->ejecQuery($this->db->query);
			return $result;
		}
	}


	public function contSeatOccupiedInService($date,$id_service)
	{
		$this->db->query = "SELECT COUNT(services_details.id) as occupied_seats FROM services_details WHERE services_details.id_services = '{$id_service}' AND services_details.services_date = '{$date}'";
		$result = $this->db->ejecQuery($this->db->query);
		return $result[0]['occupied_seats'];
	}

	public function getAllSeatsOccupiedInService($date,$id_service)
	{
		$this->db->query = "SELECT services_details.services_seat as SEAT FROM `services_details` WHERE services_details.id_services = '{$id_service}' AND services_details.services_date = '${date}'";
		$result = $this->db->ejecQuery($this->db->query);
		return $result;
	}



}



