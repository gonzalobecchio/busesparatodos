<?php 
require_once(realpath($_SERVER['DOCUMENT_ROOT'] . '\\busesParaTodos\\dirs.php'));
require_once(realpath(CLASS_PATH . 'Service/services.class.php'));
require_once(realpath(CLASS_PATH . 'Place/place.class.php'));
require_once(realpath(CLASS_PATH . 'Availability/availability.class.php'));
require_once(realpath(CLASS_PATH . 'Pagination/pagination.class.php'));
require_once(realpath(ROOT_DIR . '\\config.php'));
require_once(realpath(INC . '\\include.twig.php'));

$service = new Service();
$place = new Place();
$availability = new Availability();
$pagination = new Pagination();
/*Cantidad de registros y cantidad de paginas*/
$pagination->setCountRegisters($service->getTableName());

switch ($_REQUEST['action']) {
	case 'index': 
		$pagination->paginator($_REQUEST['page']);
		$services_array = $service->getAllServices($pagination->getInitPagination());
		$view = 'table.services.html.twig';
		$services_array_tpl = $service->getAllTlp($services_array);
		echo $twig->render(
							'index_services.html.twig', 
							[
								'view' => $view,
								'services_array_tpl' => $services_array_tpl,
								'pages' => $pagination->getPages(),
								'page_a' => $pagination->getActualPage(),
							],
						  );
	break;
	case 'create':
			# code...
	break;
	case 'delete':
			# code...
	break;
	case 'update':
			# code...
	break;
	case 'show':
		$view = 'form.newService.html.twig';
		echo $twig->render('index_services.html.twig', compact('view'));
	break;
	case 'edit':
			# code...
	break;
	case 'store':
		$id_new_service = $service->createService($_REQUEST);
		$availability->createAvailabilityForChecked($_REQUEST,$id_new_service);
	break;
	case 'getAllPlacesIndex':
		$places_array = $place->getAllPlace();
		echo $places_array; 
	break;
	case 'getAllServiceOfId':
		$array_services_of_id = $service->getAllTlp($service->getAllServiceOfId($_GET['id']));
		echo json_encode($array_services_of_id);
	break;
	/*search encargada de buscar los servicios con destino y fecha*/
	case 'search':
		$services_availability_array = $service->searchService($_REQUEST); 
		echo json_encode($service->getAllTlp($services_availability_array));
	break;
	case 'getDatesOfServiceSelected':
		/*Consulta de datos sobre servicio*/
		$array_service = $service->getDatesOfServiceSelected($_POST['id_item']);
		$field_add = [
						'contSeatOccupiedInService' => $service->contSeatOccupiedInService($_POST['data_buscador'], $_POST['id_item']),
						'ocuppied_seats_array' => $service->getAllSeatsOccupiedInService($_POST['data_buscador'], $_POST['id_item']),
					 ];		
		/*Se ingresa campo contSeatOccupiedInService el cual informa cantidad asientos ocupados*/ 
		/*Se ingresa campo ocuppied_seats_array el cual carga los asientos ocupados en un array*/ 
		$array_service_add_fields[] = array_replace($array_service[0], $field_add);
		echo json_encode($service->getAllTlp($array_service_add_fields));
	break;
	case 'setDatesServicesDetails':
		$view = $_POST['view'];
		$template = $twig->load('index.html.twig');
		echo $template->renderBlock('results', compact('view'));
		break;
	case 'getViewRenderBlockServiceInformation':
		$view = $_POST['view'];
		$template = $twig->load('index.html.twig');
		echo $template->renderBlock('results', compact('view'));
		break;
	default:
		echo 'Ha ocurrido un error';
	break;
}








// require('config.php');
// $result = $connexion->query('SELECT COUNT(*) as total_products FROM product WHERE active = 1');
// $row = $result->fetch_assoc();
// $num_total_rows = $row['total_products'];


// <?php
// if ($num_total_rows > 0) {
//     $page = false;
 
//     //examino la pagina a mostrar y el inicio del registro a mostrar
//     if (isset($_GET["page"])) {
//         $page = $_GET["page"];
//     }
 
//     if (!$page) {
//         $start = 0;
//         $page = 1;
//     } else {
//         $start = ($page - 1) * NUM_ITEMS_BY_PAGE;
//     }
//     //calculo el total de paginas
//     $total_pages = ceil($num_total_rows / NUM_ITEMS_BY_PAGE);
 
//     //pongo el numero de registros total, el tamano de pagina y la pagina que se muestra
//     echo '<h3>Numero de articulos: '.$num_total_rows.'</h3>';
//     echo '<h3>En cada pagina se muestra '.NUM_ITEMS_BY_PAGE.' articulos ordenados por fecha en formato descendente.</h3>';
//     echo '<h3>Mostrando la pagina '.$page.' de ' .$total_pages.' paginas.</h3>';
 
//     $result = $connexion->query(
//         'SELECT * FROM product p 
//         LEFT JOIN product_lang pl ON (pl.id_product = p.id_product AND pl.id_lang = 1) 
//         LEFT JOIN `image` i ON (i.id_product = p.id_product AND cover = 1) 
//         WHERE active = 1 
//         ORDER BY date_upd DESC LIMIT '.$start.', '.NUM_ITEMS_BY_PAGE
//     );

//     /*Imprime los productos limitados por pagina*/
//     if ($result->num_rows > 0) {
//         echo '<ul class="row items">';
//         while ($row = $result->fetch_assoc()) {
//             echo '<li class="col-lg-4">';
//             echo '<div class="item">';
//             echo '<h3>'.$row['name'].'</h3>';
//             ...
//             echo '</div>';
//             echo '</li>';
//         }
//         echo '</ul>';
//     }
 
//     echo '<nav>';
//     echo '<ul class="pagination">';
 
//     if ($total_pages > 1) {
//         if ($page != 1) {
//             echo '<li class="page-item"><a class="page-link" href="index.php?page='.($page-1).'"><span aria-hidden="true">&laquo;</span></a></li>';
//         }
 
//         for ($i=1;$i<=$total_pages;$i++) {
//             if ($page == $i) {
//                 echo '<li class="page-item active"><a class="page-link" href="#">'.$page.'</a></li>';
//             } else {
//                 echo '<li class="page-item"><a class="page-link" href="index.php?page='.$i.'">'.$i.'</a></li>';
//             }
//         }
 
//         if ($page != $total_pages) {
//             echo '<li class="page-item"><a class="page-link" href="index.php?page='.($page+1).'"><span aria-hidden="true">&raquo;</span></a></li>';
//         }
//     }
//     echo '</ul>';
//     echo '</nav>';
// }
// 

?>