<?php 
require_once(realpath($_SERVER['DOCUMENT_ROOT'] . '\\busesParaTodos\\dirs.php'));
include_once('../../class/bd.php');

/**
 * 
 */
class Availability
{

	private $table_name = 'availability';
	public $db;
	private $check_lunes;
	private $check_martes;
	private $check_miercoles;
	private $check_jueves;
	private $check_viernes;
	private $check_sabado;
	private $check_domingo;

	
	function __construct()
	{
		$this->db = new BD();
	}

	public function setCheckLunes($value)
	{
		$this->check_lunes = $value;
	}

	public function getCheckLunes()
	{
		return $this->check_lunes;
	}

	public function setCheckMartes($value)
	{
		$this->check_martes = $value;
	}

	public function getCheckMartes()
	{
		return $this->check_martes;
	}

	public function setCheckMiercoles($value)
	{
		$this->check_miercoles = $value;
	}

	public function getCheckMiercoles()
	{
		return $this->check_miercoles;
	}

	public function setCheckJueves($value)
	{
		$this->check_jueves = $value;
	}

	public function getCheckJueves()
	{
		return $this->check_jueves;
	}

	public function setCheckViernes($value)
	{
		$this->check_viernes = $value;
	}

	public function getCheckViernes()
	{
		return $this->check_viernes;
	}

	public function setCheckSabado($value)
	{
		$this->check_sabado = $value;
	}

	public function getCheckSabado()
	{
		return $this->check_sabado;
	}

	public function setCheckDomingo($value)
	{
		$this->check_domingo = $value;
	}

	public function getCheckDomingo()
	{
		return $this->check_domingo;
	}



	public function getTableName()
	{
		return $this->table_name;
	}

	public function createAllSetters(Array $array)
	{
		if (count($array) == 0) {
			return;
		}

		if (isset($array['btn_check_1'])) {
			$this->setCheckLunes($array['btn_check_1']);
		}

		if (isset($array['btn_check_2'])) {
			$this->setCheckMartes($array['btn_check_2']);
		}

		if (isset($array['btn_check_3'])) {
			$this->setCheckMiercoles($array['btn_check_3']);
		}

		if (isset($array['btn_check_4'])) {
			$this->setCheckJueves($array['btn_check_4']);
		}

		if (isset($array['btn_check_5'])) {
			$this->setCheckViernes($array['btn_check_5']);
		}

		if (isset($array['btn_check_6'])) {
			$this->setCheckSabado($array['btn_check_6']);
		}

		if (isset($array['btn_check_0'])) {
			$this->setCheckDomingo($array['btn_check_0']);
		}
	}

	public function createAvailabilityForChecked($array,$id_service)
	{
		// print_r('Ingresa'); die;
		try {
			$this->createAllSetters($array);
			if (isset($array['btn_check_0'])) {

				$this->db->query = "insert into {$this->getTableName()} (id_services, availability_day) values ('{$id_service}' , '{$this->getCheckDomingo()}')";
				$this->db->ejecQuery($this->db->query);

			}

			if (isset($array['btn_check_1'])) {
				$this->db->query = "insert into {$this->getTableName()} (id_services, availability_day) values ('{$id_service}' , '{$this->getCheckLunes()}')";
				$this->db->ejecQuery($this->db->query);
				
			}

			if (isset($array['btn_check_2'])) {
				$this->db->query = "insert into {$this->getTableName()} (id_services, availability_day) values ('{$id_service}' , '{$this->getCheckMartes()}')";
				$this->db->ejecQuery($this->db->query);
				
			}

			if (isset($array['btn_check_3'])) {
				$this->db->query = "insert into {$this->getTableName()} (id_services, availability_day) values ('{$id_service}' , '{$this->getCheckMiercoles()}')";
				$this->db->ejecQuery($this->db->query);
				
			}

			if (isset($array['btn_check_4'])) {
				$this->db->query = "insert into {$this->getTableName()} (id_services, availability_day) values ('{$id_service}' , '{$this->getCheckJueves()}')";
				$this->db->ejecQuery($this->db->query);
				
			}

			if (isset($array['btn_check_5'])) {
				$this->db->query = "insert into {$this->getTableName()} (id_services, availability_day) values ('{$id_service}' , '{$this->getCheckViernes()}')";
				$this->db->ejecQuery($this->db->query);
				
			}

			if (isset($array['btn_check_6'])) {
				$this->db->query = "insert into {$this->getTableName()} (id_services, availability_day) values ('{$id_service}' , '{$this->getCheckSabado()}')";
				$this->db->ejecQuery($this->db->query);
				
			}
		} catch (Exception $e) {
			echo $e->getMessage();
		} finally {
			header('Location: ?action=index');
		}
		
	}




}