<?php 
		require_once(realpath($_SERVER['DOCUMENT_ROOT'] . '\\busesParaTodos\\dirs.php'));

		require_once('./inc/include.twig.php');
		require_once('./class/Place/place.class.php');
		require_once('config.php');

		$place = new Place();
		$places = $place->getAllPlace();

		echo $twig->render('index.html.twig', compact('places'));

 ?>