window.addEventListener('DOMContentLoaded', inicio, false);

const baseURL = '\/busesParaTodos\/controllers';

function inicio() {

	/*Declaracion de Variables*/
	var urlActual = window.location;
	var selectO_newService = document.getElementById('origen_newService');
	var selectD_newService = document.getElementById('destino_newService');
	var selectO_buscador = document.getElementById('origen_buscador');
	var selectD_buscador = document.getElementById("destino_buscador");
	var btn_search_service = document.getElementById('btn-search-service');
	var date_buscador = document.querySelector("div.col input[name='date_buscador']");
	var btn_verify_dni = document.getElementById("btn_verify_dni");
	var btn_back_to_modal = document.getElementById("btn_back_to_modal");
	var btn_close_modal_2 = document.getElementById("btn_close_modal_2");
	var btn_close_modal_1 = document.getElementById("btn_close_modal_1");
	var btn_canceled_modal_1 = document.getElementById("btn_canceled_modal_1");

	var btn_save_passenger = document.getElementById("btn_save_passenger_id");

	// var btn_cart_service = document.getElementById("btn_cart_service");
	var precio = document.getElementById('precio');
	var theLastElement;
	var contBusqueda;

	//Array que especifica los valores para crear un elemento html dinamico
	let arrAtrOrigenDestino = ['place_name', 'option', 'value'];
	let arrAtrBus = ['bus_model', 'option', 'value'];
	let arrAtrOrigenBuscador = ['place_name' , 'option', 'value'];

	
	/*Modal creado e inicializado en la buqueda de servicios y asientos disponibles*/
	var myModal;


	
	
	var myModal2_inst;


	/*Verifiacion de Vistas*/


	if (typeof getViewCurent(urlActual) == 'undefined') {
		var myModal_2_new =  new bootstrap.Modal(document.getElementById('modal2'), {
			keyboard: false,
			backdrop: 'static',
		});

		// console.log('Se encuentra en view index ' +  urlActual);

		selectO_buscador.addEventListener('change', function(e) {
			loadDynamicSelectChild(baseURL + '/Services.controller/services.controller.php?action=getAllServiceOfId',selectD_buscador,e);
		}, false);

		loadSelect(baseURL + '/Services.controller/services.controller.php?action=getAllPlacesIndex','origen_buscador',arrAtrOrigenBuscador);

		btn_search_service.addEventListener('click', () => {
			sessionStorage.setItem('date_search', document.getElementById("date_buscador").value);
			getResultServicesForPlace(baseURL + '/Services.controller/services.controller.php?action=search');
		}, false);

		btn_verify_dni.addEventListener('click', () => {
			/*Verificar instancia en segunda busqueda en caso de cancel*/
			myModal_2_new = new bootstrap.Modal(document.getElementById('modal2'), {
				keyboard: false,
				backdrop: 'static',
			});
			myModal_2_new.show();
			instModal('staticBackdrop')
			.then(res=> res.hide());
			verifyPassenger();
		}, false);

		btn_back_to_modal.addEventListener('click', () => {
			myModal_2_new.hide();
			instModal('staticBackdrop')
			.then(res=> res.show());
			cleanFormModal();
			console.log(document.getElementById("panel_btn_passenger").childNodes[1]);
			cleanPanelBtn(document.getElementById("panel_btn_passenger"));
		}, false);

		btn_close_modal_2.addEventListener('click', () => {
			cleanFormModal();
		}, false);

		btn_close_modal_1.addEventListener('click', () => {
			document.getElementById("dni_modal_seat").value = "";
		}, false);

		btn_canceled_modal_1.addEventListener('click', () => {
			document.getElementById("dni_modal_seat").value = "";
		}, false);

		// btn_accept_passenger_dates.addEventListener('click', () => {
		// 	myModal_2_new.hide();
		// 	instModal('staticBackdrop').then(res => console.log(res));
		// 	document.getElementById("dni_modal_seat").value = "";
		// 	cleanFormModal();
		// 	setDatesServicesDetails();
		// }, false);




	}


	if (getViewCurent(urlActual) == 'table-services') {
		
	}

	if (getViewCurent(urlActual) == 'form-new-service') {
		document.getElementById('form_new_service').addEventListener('submit', verifyForm, false);
		loadSelect(baseURL + '/Place.controller/place.controller.php?action=getAllPlace', 'origen_newService', arrAtrOrigenDestino);
		loadSelect(baseURL + '/Place.controller/place.controller.php?action=getAllPlace', 'destino_newService', arrAtrOrigenDestino);
		loadSelect(baseURL + '/Bus.controller/bus.controller.php?action=getAllBus', 'bus', arrAtrBus);
	}

}


/***********************************************Funcionalidades******************************************************************************/


const instModal = (modal) => {
	return new Promise((resolve,reject) => {
		let insta_modal = document.getElementById(modal);
		let insta_modal_result = bootstrap.Modal.getInstance(insta_modal);
		resolve(insta_modal_result);
	});
}


/*Funcion que se ejecuta luego de que usuario acepta en modal de busqueda de pasajero en caso de existir*/
/*En este paso ya tenemos todos los datos del pasajero para poder realizar la confirmacion del pasaje*/
function setDatesServicesDetails() {
	let formData = new FormData();
	formData.append('view', 'services.details.html.twig');
	const myInit = {
		method: 'POST',
		body: formData,
	};

	let url = baseURL + "/Services.controller/services.controller.php?action=setDatesServicesDetails";

	fetch(url,myInit)
	.then(response => {
		if (!response.ok) {
			throw new Error(`HTTP error! status: ${response.status}`);
		}
		return response.status == 200 ? response.text() : console.log(`Response HTTP: ${response.statusText} - ${response.status}`);
	})
	.then(data =>  {
		var passenger_id, item_services_id, date_search;
		document.getElementById("title_info_service").innerText = 'Datos Pasaje';
		/*Devolucion del bloque renderizado*/
		document.getElementById("block_information_service").innerHTML = data;
		let passenger_dates = JSON.parse(sessionStorage.getItem('passenger_dates'));
		let seat_selected = sessionStorage.getItem('seat_selected');
		let item_services_data = JSON.parse(sessionStorage.getItem('item_services_data'));
		date_search = sessionStorage.getItem("date_search");
		// console.log(passenger_dates);
		passenger_id = passenger_dates[0].id;
		item_services_id = item_services_data[0].id;
		getDates(item_services_data)
		.then(dates_services_selected => {
			console.log(dates_services_selected[0].id_o);
			document.getElementById("origen_services_details").innerText = dates_services_selected[0].id_o;
			document.getElementById("destino_services_details").innerText = dates_services_selected[0].id_d;
			document.getElementById("precio_services_details").innerText = dates_services_selected[0].PRECIO;
			document.getElementById("seat_services_details").innerText = seat_selected;
			document.getElementById("hs_services_details").innerText = dates_services_selected[0].HORA_SALIDA;
			document.getElementById("hll_services_details").innerText = dates_services_selected[0].HORA_LLEGADA;
		});

		getDates(passenger_dates)
		.then(passenger_dates => {
			console.log(passenger_dates);
			document.getElementById("name_services_details").innerText = passenger_dates[0].person_name;
			document.getElementById("last_name_services_details").innerText = passenger_dates[0].person_lastname;
			document.getElementById("dni_services_details").innerText = passenger_dates[0].person_dni;
			document.getElementById("email_services_details").innerText = passenger_dates[0].person_email;
		});


		let dates_services = [{
			"seat" : seat_selected,
			"item_services_id" : item_services_id,
			"passenger_id" : passenger_id,
			"date_search" : date_search,
		}];

		sessionStorage.removeItem("passenger_dates");
		sessionStorage.removeItem("seat_selected");
		sessionStorage.removeItem("item_services_data");
		// sessionStorage.clear();

		/*button cancel buy*/
		let span_text_cancela_buy = document.createTextNode("cancel");
		let span_element_cancel = document.createElement("span");
		span_element_cancel.setAttribute("class", "material-icons");
		span_element_cancel.appendChild(span_text_cancela_buy);
		let button_cancel_buy = document.createElement("button");
		button_cancel_buy.setAttribute("class","btn btn-danger");
		button_cancel_buy.setAttribute("id","btn_cancel_buy");
		button_cancel_buy.appendChild(span_element_cancel);
		document.getElementById("btn_panel_left").appendChild(button_cancel_buy);
		/*button cancel buy*/


		/*Creacion btn compra/confirmacion pasaje*/
		let span_text = document.createTextNode("shopping_cart");
		let span_element = document.createElement('span');
		span_element.setAttribute("class", "material-icons");
		span_element.appendChild(span_text);

		let button_cart = document.createElement("button");
		button_cart.setAttribute("class","btn btn-primary");
		button_cart.setAttribute("id","btn_cart_service");
		button_cart.appendChild(span_element);
		document.getElementById("btn_panel_right").appendChild(button_cart);
		let btn_cart_services = document.getElementById("btn_cart_service");
		btn_cart_services.addEventListener('click', () => {

			let view = "busAvailabilitySeat.html.twig";
			formData = new FormData();
			formData.append('id_item', item_services_id);
			formData.append('view', view);
			formData.append('data_buscador', date_search);

			const myInit = {
				method: 'POST',
				body:  formData,
			};
			
			cleanTableBus(document.getElementById("maqueta_bus"));
			let clean =  ["title_maqueta_bus"];
			cleanValuesElement(clean);
			getDatesServiceSelectes(myInit)
			.then(data => {
				// console.log(data);
				/*Envio a session los datos del item (servicio seleccionado)*/
				sessionStorage.setItem('item_services_data', JSON.stringify(data));
				/*Calculo de los asientos disponibles*/
				let seat_availables = parseInt(data[0].bus_seats) - parseInt(data[0].contSeatOccupiedInService);
				const arrayElementClean = [
				'title_maqueta_bus',
				'title_seat_available',
				'title_info_service',
				];
				/*Limpiar values elements*/
				cleanValuesElement(arrayElementClean);
				document.getElementById('title_maqueta_bus').appendChild(document.createTextNode(`Asientos Unidad : ${data[0].bus_seats}`));
				// document.getElementById('title_seat_available').appendChild(document.createTextNode(`Asientos Disponibles: ${seat_availables}`));
				document.getElementById('bloque_maqueta_bus').setAttribute("class", "visible");
				document.getElementById("title_info_service").appendChild(document.createTextNode(`Informacion servicio ${data[0].id_o} - ${data[0].id_d}`));
				const seatsIterable = data[0];
				printBus(seatsIterable);
				getViewRenderBlockServiceInformation()
				.then(result => {
					document.getElementById("block_information_service").innerHTML = result; 
					document.getElementById("origen_block_information").innerText = data[0].id_o;
					document.getElementById("destino_block_information").innerText = data[0].id_d;
					document.getElementById("precio_block_information").innerText = data[0].PRECIO;
					seats_avaibles_block_information = document.getElementById("seats_avaibles_block_information");
					seats_avaibles_block_information.setAttribute("class","text-dark bg-success");
					if (seat_availables <= 10) {
						seats_avaibles_block_information.setAttribute("class","text-dark bg-danger")	
					}
					seats_avaibles_block_information.innerText = seat_availables;
					
				});
			});
			saveServicesDetails(dates_services);

		}, false);
		
		
	});

}


/*Envio de datos a controlador para insertar el servicio en base de datos*/
/*Verificacion de Datos ante de ingresar a la base desde js y PHP*/
function saveServicesDetails(dates_services) {
	const myInit = {
		method: "POST",
		body : JSON.stringify(dates_services),
		headers : {
			// 'Content-Type': 'application/json',
			'Content-Type': 'application/x-www-form-urlencoded',
		},
	}
	let url = baseURL + "/Services.Details.controller/services.details.controller.php?action=insertServiceDetails";
	fetch(url, myInit)
	.then(response => {
		if (!response.ok) {
			throw new Error(`HTTP error! status: ${response.status}`);
		}
		return response.status == 200 ? response.json() : console.log(`Response HTTP: ${response.statusText} - ${response.status}`);
	})
	.then(response => {
		swal({
			title: "Información importante",
			text: "El pasaje ha sido reservado, estará llegando a su correo en unos instantes",
			icon: "success",
			button: "Aceptar",
		});
	});
}


const getDates = (dates) => {
	return new Promise((resolve,reject) => {
		resolve(dates);
	})
} 



/*Return true si el asiento se encuentra entre los ocupados*/
function pintSeatsInitSearch(seats,seat_n) {
	for (var i = seats.length - 1; i >= 0; i--) {
		if (seats[i].SEAT == seat_n) {
			return true;
		}
		
	}
}

function cleanPanelBtn(panel_btn) {
	// console.log(panel_btn.childNodes);
	for (var i = panel_btn.childNodes.length - 1; i >= 0; i--) {
		if (panel_btn.childNodes[i].id != "btn_back_to_modal") {
			 panel_btn.removeChild(panel_btn.childNodes[i]);
		}
	}
}


/*Function limpiar form cuando vuelvo, cancelo o cierro desde X*/
function cleanFormModal(){
	let form = document.getElementById('form-modal-passenger');
	console.log(form.childNodes);
	for (var i = form.childNodes.length - 1; i >= 0; i--) {
		form.removeChild(form.childNodes[i]);
	}
}


/*Funcion creacion form datos pasajero*/
function printFormPassengerFind(response, data = null) {
	/*Creacion de labels y sus nombres*/
	// console.log(data[0].person_name);
	let label_name = document.createElement('label'); 
	let label_lastName = document.createElement('label'); 
	let label_dni = document.createElement('label'); 
	let label_email = document.createElement('label');

	// /*Creando btn para modal*/
	// let btn_modal_passanger = document.createElement('button');
	// btn_modal_passanger.setAttribute("class","btn btn-primary");
	// /*Creando btn para modal*/

	let text_label_name = document.createTextNode('Nombre');
	let text_label_lastName = document.createTextNode('Apellido');
	let text_label_dni = document.createTextNode('DNI');
	let text_label_email = document.createTextNode('Email');
	label_name.appendChild(text_label_name);
	label_lastName.appendChild(text_label_lastName);
	label_dni.appendChild(text_label_dni);
	label_email.appendChild(text_label_email);


	/*Creacion de INPUTS ELEMENTS*/
	let input_name = document.createElement('input');
	let input_lastName = document.createElement('input');
	let input_dni = document.createElement('input');
	let input_email = document.createElement('input');

	input_name.setAttribute('class', 'form-control');
	input_lastName.setAttribute('class', 'form-control');
	input_dni.setAttribute('class', 'form-control');
	input_email.setAttribute('class', 'form-control');

	input_name.setAttribute('type', 'text');
	input_lastName.setAttribute('type', 'text');
	input_dni.setAttribute('type', 'text');
	input_email.setAttribute('type', 'text');

	input_name.setAttribute('id', 'name_modal');
	input_lastName.setAttribute('id', 'lastName_modal');
	input_dni.setAttribute('id', 'dni_modal');
	input_email.setAttribute('id', 'email_modal');
	input_email.setAttribute('type', 'email');

	/*Creacion de btn accept o save passenger*/
		let btn_save_or_accept = document.createElement("button");
		btn_save_or_accept.setAttribute("type","button");
		btn_save_or_accept.setAttribute("class","btn btn-primary");
		let text_btn_save_or_accept;
	/*Creacion de btn*/


	if (response === 1) {
		/*Pasajero encontrado*/
		input_name.setAttribute('value',data[0].person_name);
		input_name.setAttribute('disabled','disabled')
		input_lastName.setAttribute('value',data[0].person_lastname);
		input_lastName.setAttribute('disabled','disabled');
		input_dni.setAttribute('value',data[0].person_dni);
		input_dni.setAttribute('disabled','disabled')
		input_email.setAttribute('value',data[0].person_email);
		input_email.setAttribute('disabled','disabled');

		let text_btn_save_or_accept = document.createTextNode("Aceptar");
		btn_save_or_accept.appendChild(text_btn_save_or_accept);
		btn_save_or_accept.setAttribute("id","btn_accept_passenger_dates");
		/*Agrego el boton al panel de btones*/
		document.getElementById("panel_btn_passenger").appendChild(btn_save_or_accept);
		/*Inicializo el boton aceptar*/
		btn_save_or_accept.addEventListener('click', () => {
			alert("Hello pasajero Encontrado");
			myModal_2_new =  bootstrap.Modal.getInstance(document.getElementById("modal2"))
			myModal_2_new.hide();
			instModal('staticBackdrop').then(res => console.log(res));
			document.getElementById("dni_modal_seat").value = "";
			cleanFormModal();
			cleanPanelBtn(document.getElementById("panel_btn_passenger"));
			setDatesServicesDetails();
		}, false);

	}else{
		/*Pasajero no encontrado*/
		input_name.setAttribute('value',"");
		input_name.setAttribute("autocomplete","off");
		input_lastName.setAttribute('value',"");
		input_lastName.setAttribute("autocomplete","off");
		input_dni.setAttribute('value',"");
		input_dni.setAttribute("autocomplete","off");
		input_email.setAttribute("value","");
		input_email.setAttribute("autocomplete","off");


		let text_btn_save_or_accept = document.createTextNode("Guardar");
		btn_save_or_accept.appendChild(text_btn_save_or_accept);
		btn_save_or_accept.setAttribute("id","btn_save_passenger_id");
		/*Agrego el boton al panel*/
		document.getElementById("panel_btn_passenger").appendChild(btn_save_or_accept);
		/*Inicializo el boton Guardar*/
		btn_save_or_accept.addEventListener('click', () => {
			//Oculta el modal
			// myModal_2_new =  bootstrap.Modal.getInstance(document.getElementById("modal2"))
			// myModal_2_new.hide();
			let name_modal = document.getElementById("name_modal").value;
			console.log(name_modal);
			let lastName_modal = document.getElementById("lastName_modal").value;
			let dni_modal = document.getElementById("dni_modal").value;
			let email_modal = document.getElementById("email_modal").value;
			formData = new FormData();
			formData.append('name_modal', name_modal);
			formData.append('lastName_modal', lastName_modal);
			formData.append('dni_modal', dni_modal);
			formData.append('email_modal',email_modal);

			const myInit = {
				method: "POST",
				body: formData,
			}
			setDatesNewPassenger(myInit)
			.then(res => {
				console.log(res);
			});

		}, false);



	}

	/***************************************************************************************************************/

	/*Creacion de DIV y Adjuntamiento de label e imputs*/
	let div_name = document.createElement('div');
	div_name.setAttribute('class', 'mb-3');
	let div_lastName = document.createElement('div');
	div_lastName.setAttribute('class', 'mb-3');
	let div_dni = document.createElement('div');
	div_dni.setAttribute('class', 'mb-3');
	let div_email = document.createElement('div');
	div_email.setAttribute('class', 'mb-3');

	div_name.appendChild(label_name);
	div_name.appendChild(input_name);
	div_lastName.appendChild(label_lastName);
	div_lastName.appendChild(input_lastName);
	div_dni.appendChild(label_dni);
	div_dni.appendChild(input_dni);
	div_email.appendChild(label_email);
	div_email.appendChild(input_email);
	document.getElementById('form-modal-passenger').appendChild(div_name);
	document.getElementById('form-modal-passenger').appendChild(div_lastName);
	document.getElementById('form-modal-passenger').appendChild(div_dni);
	// if (response === 1) {
	document.getElementById('form-modal-passenger').appendChild(div_email);
	// }
	
	
}

const setDatesNewPassenger = (myInit) => {
	return new Promise((resolve, reject) => {
		const url = baseURL + "/Passenger.controller/passenger.controller.php?action=setDatesNewPassenger";
		fetch(url,myInit).
		then(result => {
			resolve(result);
		});
	});
}



const getAllPassenger = (dni) => {
	return new Promise((resolve,reject) => {
		let action = 'getAllPassenger';
		const url = `${baseURL}\\Passenger.controller\\passenger.controller.php?action=${action}`;
		let data = new FormData();
		data.append('dni',dni);
		let result;
		const myInit = {
			method: 'POST',
			body: data,
		};

		fetch(url,myInit)
		.then(response => {
			if (!response.ok) {
				throw new Error(`HTTP error! status: ${response.status}`);
			}
			return response.status == 200 ? response.json() : console.log(`Response HTTP: ${response.statusText} - ${response.status}`);
		})
		.then(response => {
			resolve(response);
		});
	});
}

function verifyPassenger() {
	let dni_modal_seat = document.getElementById("dni_modal_seat").value;
	data = new FormData();
	data.append('dni', dni_modal_seat)
	const myInit = {
		method: "POST",
		body: data,
	};
	
	fetch(`${baseURL}\\Passenger.controller\\passenger.controller.php?action=verifyPassenger`,myInit)
	.then(response => {
		if (!response.ok) {
			throw new Error(`HTTP error! status: ${response.status}`);
		}

		return response.status == 200 ? response.json() : console.log(`Response HTTP: ${response.statusText} - ${response.status}`);
	})
	.then(response => {
		if (!parseInt(response)) {
			console.log("Pasajero NO encontrado");
			let encontrado = 0;
			document.getElementById("title_modal_passenger_dates").innerText = "Datos Pasajero";
			printFormPassengerFind(encontrado);
			

		}else{
			console.log("Pasajero ENCONTRADO");	
			let encontrado = 1;
			document.getElementById("title_modal_passenger_dates").innerText = "Datos Pasajero";
			getAllPassenger(dni_modal_seat)
			.then(data => {
				console.log(data);
				sessionStorage.setItem('passenger_dates', JSON.stringify(data))
				printFormPassengerFind(encontrado,data);
			});
		}

	})
	.catch(error => {
		console.log('Hubo un problema con la petición Fetch:' + error.message);
	});

}




const getData = () => {
	return new Promise((resolve,reject) => {
		resolve(data);
		console.log(data);
	});
}

/*Terminar de hacer la consulta para verificar el bus(disponibilida de asientos) 
por el servicio y tambien traer los datos del services antes de ingresar un detalle*/
/*Funcion encargadad inicializar el evento click de cada item o servicio, en la cual
se cargan o dibujan todos los asientos (libres u ocupados)*/

/*param id -> ingresa el id de item, el cual se utiliza para buscar todo los referido al servicio*/
function initItemService(id) {
	let id_item = id;
	let view = 'busAvailabilitySeat.html.twig';
	// Inicializacion evt click de btn items - seats
	document.getElementById(id).addEventListener("click", () => {
		formData = new FormData();
		formData.append('id_item', id_item.split('_',2)[1]);
		formData.append('view', view);
		formData.append('data_buscador', document.getElementById('date_buscador').value);

		const myInit = {
			method: 'POST',
			body:  formData,
		};


		/***Probando****/
		cleanTableBus(document.getElementById("maqueta_bus"));
		/**********/

		/*De aca hacia abajo meterlo en una promesa*/
		/*Busqueda datos del servicio seleccionado al presionar boton view*/

		getDatesServiceSelectes(myInit)
		.then(data => {
			console.log(data);
			/*Envio a session los datos del item (servicio seleccionado)*/
			sessionStorage.setItem('item_services_data', JSON.stringify(data));
			/*Calculo de los asientos disponibles*/
			let seat_availables = parseInt(data[0].bus_seats) - parseInt(data[0].contSeatOccupiedInService);
			const arrayElementClean = [
				'title_maqueta_bus',
				'title_seat_available',
				'title_info_service',
			];
			/*Limpiar values elements*/
			cleanValuesElement(arrayElementClean);
			document.getElementById('title_maqueta_bus').appendChild(document.createTextNode(`Asientos por Unidad : ${data[0].bus_seats}`));
			// document.getElementById('title_seat_available').appendChild(document.createTextNode(`Asientos Disponibles: ${seat_availables}`));
			document.getElementById('bloque_maqueta_bus').setAttribute("class", "visible");
			document.getElementById("title_info_service").appendChild(document.createTextNode(`Informacion servicio ${data[0].id_o} - ${data[0].id_d}`));

			getViewRenderBlockServiceInformation().
			then(result => {
				// console.log(result);
				document.getElementById("block_information_service").innerHTML = result; 
				document.getElementById("origen_block_information").innerText = data[0].id_o;
				document.getElementById("destino_block_information").innerText = data[0].id_d;
				document.getElementById("precio_block_information").innerText = data[0].PRECIO;


				// document.getElementById("seats_avaibles_block_information").innerText = seat_availables;

				seats_avaibles_block_information = document.getElementById("seats_avaibles_block_information");
				seats_avaibles_block_information.setAttribute("class","text-dark bg-success");
				// console.log(seat_availables < 30);
				if (seat_availables <= 10) {
					seats_avaibles_block_information.setAttribute("class","text-dark bg-danger")	
				}
				seats_avaibles_block_information.innerText = seat_availables;

			});

			/*Tratar el bloque de informacion de servicio con promesa*/
			const seatsIterable = data[0];
			printBus(seatsIterable);
		});



		
	}, false);
}

const getViewRenderBlockServiceInformation = () => {
	return new Promise((resolve, reject) => {
		data = new FormData();
		data.append('view', 'view.service.information.html.twig');
		myInit  = {
			method: "POST",
			body: data,
		}
		let url = baseURL + "/Services.controller/services.controller.php?action=getViewRenderBlockServiceInformation";
		fetch(url,myInit).then(result => {
			if (!result.ok) {
				throw new Error(`HTTP error! status: ${result.status}`);
			}
			resolve(result.status == 200 ? result.text() : console.log(`Response HTTP: ${result.statusText} - ${result.status}`));
		})
	})
}


/*Funcion encargada de traer datos del servicio seleccionado*/
const getDatesServiceSelectes = (myInit) => { 
	return new Promise((resolve,reject)=>{
		fetch(baseURL + '/Services.controller/services.controller.php?action=getDatesOfServiceSelected', myInit)
		.then(response => {
			if (!response.ok) {
				throw new Error(`HTTP error! status: ${response.status}`);
			}
			resolve(response.status == 200 ? response.json() : console.log(`Response HTTP: ${response.statusText} - ${response.status}`));
		})
	})
}


/*Funcion encargada de vaciar los elementos con texto*/
function cleanValuesElement (array_ids_element) {
	for (var i = array_ids_element.length - 1; i >= 0; i--) {	
		document.getElementById(array_ids_element[i]).innerText = "";
	}
}


/*Dibujo asientos bus*/
function printBus (seatsIterable) {
	/*Se dibujan todos los asientos de la unidad-bus*/
	const seatsForRow = 4; /*Asiento por fila*/
			// let seatPrint = 0; /*Asientos dibujados*/
			let tbody = document.getElementById("maqueta_bus");
			var seat_n;
			let seatPrint = 0; /*Asientos dibujados*/
			for (var i = seatPrint; i <= seatsIterable.bus_seats; i++) {
				let row = document.createElement('tr');
				for (var j = 1 ; j <= seatsForRow; j++) {
					if (seatPrint == seatsIterable.bus_seats ) {
						return;
					}
					seat_n = seatPrint + 1;
					let td_seat = document.createElement('td');
					let a_seat = document.createElement('a');
					let span_seat = document.createElement('span');
					let span_text = document.createTextNode('airline_seat_recline_extra');
					span_seat.setAttribute('class','material-icons');
					span_seat.setAttribute('style', 'color: green;');
					/*Obj que posee los asientos ocupados del servicio*/
					/*seat_n corresponde al numero de asiento*/
					if (pintSeatsInitSearch(seatsIterable.ocuppied_seats_array,seat_n)) {
						// console.log(seat_n);
						span_seat.setAttribute('style', 'color: red;');
					}
					

					span_seat.setAttribute('id',`seat_${seat_n}`);
					span_seat.appendChild(span_text);	
					a_seat.setAttribute('href','#');
					a_seat.setAttribute('id', seat_n);

					a_seat.appendChild(span_seat);
					td_seat.appendChild(a_seat);

					
					/*Se iniciliza el evento click al presionar el asiento*/
					if (!pintSeatsInitSearch(seatsIterable.ocuppied_seats_array,seat_n)) {
						a_seat.addEventListener("click", (evt) => {
							myModal = new bootstrap.Modal(document.getElementById('staticBackdrop'), {
								keyboard: false,
								backdrop: 'static',
							})
							sessionStorage.setItem('seat_selected', a_seat.getAttribute('id'));
							document.getElementById("title_modal").innerText = `Detalle Asiento ${a_seat.getAttribute('id')}`;
							document.getElementById("origen_modal").innerText = `${seatsIterable.id_o}`;
							document.getElementById("destino_modal").innerText = `${seatsIterable.id_d}`;
							document.getElementById("precio_modal").innerText = `$${`${seatsIterable.PRECIO}`}`;
							myModal.show();
						},false);
					}
					

					row.appendChild(td_seat);
					tbody.appendChild(row);
					seatPrint++;
				}	
			}/*endfor*/	 
		}


		/*Funcion encargada de enviar la consulta de busqueda de servicios a controller*/
		/*Encargada de armar la tabla de resultados(JSON), con componentes html*/
		/*Añadir elementos a la tabla generica que se creara*/

	
function getResultServicesForPlace(url) {
	let body_content = document.getElementById('body_content');
	let block_information_service = document.getElementById("block_information_service");
	cleanBodyTable(body_content);
	cleanTableBus(document.getElementById("maqueta_bus"));
	block_information_service.innerHTML = "";
	document.getElementById("title_info_service").innerText = "";
	let values = ["title_maqueta_bus"];
	cleanValuesElement(values);

	let id_o = document.getElementById('origen_buscador').value;
	let id_d = document.getElementById('destino_buscador').value;
	let date_buscador = document.getElementById('date_buscador').value;
	// let table_content = document.getElementById('table_content');

	//Contenedor que posee la tabla con resultados
	let content_result_search = document.getElementById('content_result_search');
	

	let formData = new FormData();
	formData.append('id_o',id_o);
	formData.append('id_d',id_d);
	formData.append('date_buscador',date_buscador);

	xmlInst = new XMLHttpRequest();
	xmlInst.open('POST', url, true);
	xmlInst.send(formData);
	xmlInst.onreadystatechange = (evt) => {
		if (evt.target.readyState == 4) {
			if (evt.target.status == 200) {
				let objJson = JSON.parse(evt.target.responseText);
				// console.log(objJson);
				if (objJson.length != 0) {

					if (content_result_search.hasAttribute('class')) {
						content_result_search.setAttribute('class','visible');
					}

					for (var i = objJson.length - 1; i >= 0; i--) {

						let tr = document.createElement('tr');

						let td_origen = document.createElement('td');
						let text_origen = document.createTextNode(objJson[i].id_o);
						td_origen.appendChild(text_origen);

						let td_destino = document.createElement('td');
						let text_destino = document.createTextNode(objJson[i].id_d);
						td_destino.appendChild(text_destino);


						let td_horaSalida = document.createElement('td');
						let text_horaSalida = document.createTextNode(objJson[i].HORA_SALIDA);
						td_horaSalida.appendChild(text_horaSalida);


						let td_horaLlegada = document.createElement('td');
						let text_horaLlegada = document.createTextNode(objJson[i].HORA_LLEGADA);
						td_horaLlegada.appendChild(text_horaLlegada);


						let td_precio = document.createElement('td');
						let text_precio = document.createTextNode(objJson[i].SERVICE_PRECIO);
						td_precio.appendChild(text_precio);

						let td_service_estate = document.createElement('td');
						let text_service_estate = document.createTextNode(objJson[i].service_estate);
						td_service_estate.appendChild(text_service_estate);


						/*td->a(setAtribute)->span*/
						let td_option = document.createElement('td');
						let a_option = document.createElement('a');
						let span_option = document.createElement('span');
						let span_text;


						if (objJson[i].service_estate_int == 1) {
							tr.setAttribute("class", "table-success");
							a_option.setAttribute('id',`item_${objJson[i].ID_SERVICIO}`);
							a_option.setAttribute('href','#');
							span_option.setAttribute('class', "material-icons");
							span_text = document.createTextNode("preview");

						}

						if (objJson[i].service_estate_int != 1) {
							tr.setAttribute("class", "table-danger");
							a_option.setAttribute('id',`item_${objJson[i].ID_SERVICIO}`);
							a_option.setAttribute('href',`#`);
							span_option.setAttribute('class', "material-icons");
							span_text = document.createTextNode("done_all");

						}


						span_option.appendChild(span_text);
						a_option.appendChild(span_option);
						td_option.appendChild(a_option);


						tr.appendChild(td_origen);
						tr.appendChild(td_destino);
						tr.appendChild(td_horaSalida);
						tr.appendChild(td_horaLlegada);
						tr.appendChild(td_precio);
						tr.appendChild(td_service_estate);
						tr.appendChild(td_option);
						body_content.appendChild(tr);
						initItemService(`item_${objJson[i].ID_SERVICIO}`);
					}

				}else{
					console.log(document.getElementById("maqueta_bus").childNodes);
					cleanTableBus(document.getElementById("maqueta_bus"));
					block_information_service.innerHTML = "";
					document.getElementById("title_info_service").innerText = "";
					let values = ["title_maqueta_bus"];
					cleanValuesElement(values);

					swal({
						title: "Información!",
						text: "Sin servicio disponible en esa fecha ='(",
						icon: "warning",
						button: "Aceptar",
					});

					if (content_result_search.hasAttribute('class')) {
						content_result_search.setAttribute('class','invisible');
					}
				}


			}
		}
	}
}



/*Prueba*/

function cleanTableBus (tbody) {
	if (tbody.childNodes.length != 0) {
		for (var i = tbody.childNodes.length - 1; i >= 0; i--) {
			tbody.removeChild(tbody.childNodes[i]); 
		}
	}else{
		return;
	}
	
}

/*Prueba*/


/*Funcion encargada de reestablecer resultados representados en tabla por cada busqueda*/
function cleanBodyTable(tbody) {
	// Verificar cuando solo hay un servicio
	// console.log(tbody.childNodes.length);
	if (tbody.childNodes.length == 1) {
		// console.log("Primera Busqueda - Cantidad de Elementos: " + tbody.childNodes.length);
		// console.log(tbody.childNodes[0].nodeType);
		if (tbody.childNodes[0].nodeType = Node.ELEMENT_NODE) {
			tbody.removeChild(tbody.childNodes[0]);
		}
	}
	if (tbody.childNodes.length != 1) {
		// console.log("No es primera Busqueda - Cantidad de Elementos: " + tbody.childNodes.length);
		let cant_elements_delete = tbody.childNodes.length;
		// console.log(tbody.childNodes);
		for (var i = cant_elements_delete - 1; i >= 0; i--) {
			tbody.removeChild(tbody.childNodes[i]);
		}

	}

}

/*Funcion que devuelve view actual*/
function getViewCurent(urlActual){
	let viewCurrent = urlActual.search.split('view=',2)[1];
	return viewCurrent;

}

function verifyForm(evt) {
	let msje = "Error en los datos del Formulario";
	if (!verifyPrice() || verifyEqualOrgDes_static() || !verifyHours()) {
		evt.preventDefault();
	}
	
	swal({
		title: "Exelente",
		text: "Servicio Registrado!!!",
		icon: "success",
		button: "Aceptar!",
	});


}

//Devuelve true si los selectores son iguales
function verifyEqualOrgDes_static() {
	let msje ="";
	let selector_origen_newService = document.getElementById('origen_newService')[document.getElementById('origen_newService').selectedIndex].text;
	let selector_destino_newService = document.getElementById('destino_newService')[document.getElementById('destino_newService').selectedIndex].text;
	if (selector_origen_newService == selector_destino_newService) {
		msje="Origen igual a Destino";
		console.log(msje);
		alert(msje);
		return true;
	}
	return false;
}

//Falso si no es un numero o menor a cero o no pertenece a number o se encuentra vacio el input
function verifyPrice() {
	let number = Number(precio.value);
	let msje = "";
	// console.log(Number.isSafeInteger(number));
	// console.log(number);
	if (Number.isNaN(number)) {
		msje = `El valor precio: ${precio.value} ingresado no corresponde a un numero`;
		console.log(msje);
		alert(msje);	
		return false ;
	}else{
		if (!(number instanceof Number)) {
			if (number < 0) {
				msje = `El valor: ${precio.value} ingresado es negativo`;
				alert(msje);
				console.log(msje);
				return false;
			}
			if (number == 0) {
				msje = `El valor: ${precio.value} es cero o no ingresan valor`;
				console.log(msje);
				alert(msje);
				return false;
			}
		}
	}
	return true;

}

//Retorna el elemento seleccionado en objecto select
function lastSelectIndex(evt) {
	if (evt.target.id == 'origen_newService') {
		theLastElement = evt.target[evt.target.selectedIndex].text;
	}
	console.log(theLastElement);
	// return theLastElement;
}



//Retorna falso si el origen y el destino son iguales (Dinamico)
function verifyEqualOrgDes(evt) {
	let selector_activo; /*Variable con string selected en lista*/
	let selector_origen_newService = document.getElementById('origen_newService')[document.getElementById('origen_newService').selectedIndex].text;
	let selector_destino_newService = document.getElementById('destino_newService')[document.getElementById('destino_newService').selectedIndex].text;
	
	if (evt.target.id == 'origen_newService') {
		selector_activo = evt.target[evt.target.selectedIndex].text;
		if (selector_activo == selector_destino_newService) {
			console.log('Origen = Destino');
			return false;
		}
		console.log('Origen dist Destino');
	}

	
	if (evt.target.id == 'destino_newService') {
		selector_activo = evt.target[evt.target.selectedIndex].text;
		if (selector_activo == selector_origen_newService) {
			console.log('Destino = Origen');
			// alert('Destino = Origen');
			return false;
			
		}
		console.log('Destino dist Origen');		
	}

}


//Retorna falso si las horas son iguales o si no se cargaron los horarios
function verifyHours() {
	let hCompletaS = document.querySelector('div.col input[id=service_hs]').value.split(':',2);
	let hCompletaLl = document.querySelector('div.col input[id=service_hll]').value.split(':',2);
	// console.log(hCompletaS.length);
	if (hCompletaS.length == 1|| hCompletaLl.length == 1) {
		alert('Complete los horarios')
		return false;
	}else{
		let hS = parseInt(hCompletaS[0] ), minS = parseInt(hCompletaS[1]);
		let hLl = parseInt(hCompletaLl[0]), minLl = parseInt(hCompletaLl[1]);
		let horaSalida = new Date(0,0,0,hS,minS,0,0); 
		let horaLlegada = new Date(0,0,0,hLl,minLl,0,0);
		// console.log(horaSalida.getTime() - horaLlegada.getTime());
		let result = horaSalida.getTime() - horaLlegada.getTime();
		/*En caso de < 0 Llega en el mismo dia  --- En caso de > 0 llega al dia siguiente*/
		// console.log(result);
		if (result == 0) {
			console.log('Horarios iguales - Cambie alguno de los dos');	
			alert('Horarios iguales - Cambie alguno de los dos');
			return false;
		}
		return true;
	}

}


function cargarProvincias() {
	let xmlInst = new XMLHttpRequest();
	xmlInst.open('GET', 'server.php?dato=provincias', true);
	xmlInst.send(null);
	xmlInst.onreadystatechange = procesoPeticion;
}


function procesoPeticion(evt) {
	if (evt.target.readyState == 4) {
		if (evt.target.status != 200) {
			console.log('Ha ocurrido un error: ' + evt.target.statusText);
		}
		// console.log(JSON.parse(evt.target.responseText));
		let obj = JSON.parse(evt.target.responseText);
		cargarSelectProvincias(obj);
		
	}else{
		console.log('Peticion estado prov: ' + evt.target.readyState);
		// console.log((evt.target.responseText));

	}
}


function cargarSelectProvincias(obj) {
	if (typeof obj === 'object') {
		let padreSelect = document.getElementById('provincias');
		for (var i = obj.provincias.length - 1; i >= 0; i--) {
			let nodoTexto = document.createTextNode(obj.provincias[i].nombre);
			let nodoElemento = document.createElement('option');
			nodoElemento.appendChild(nodoTexto);
			nodoElemento.setAttribute('value',obj.provincias[i].id);
			padreSelect.appendChild(nodoElemento);

		}
	}
	return false;
}


function cargarLocalidades() {
	let select = document.getElementById('localidades');
	limpiarSelect(select);
	let xmlInst = new XMLHttpRequest();
	xmlInst.open('GET', 'server.php?dato=localidades', true);
	xmlInst.send(null);
	xmlInst.onreadystatechange = (evt)=>{
		if (evt.target.readyState == 4) {
			if (evt.target.status != 200) {
				console.log(evt.target.statusText + 'func: procesoPeticionLoca');
			}else{
				let obj = JSON.parse(evt.target.responseText);
			// console.log('Caso de exito: ' + obj);		
			teDescargoLaLocalidades(obj);
			xmlInst.abort();
		}
	}else{
		console.log(evt.target.readyState);
	}
};
}


function limpiarSelect(elemento) {
	for (var i = elemento.length - 1; i >= 0; i--) {
		elemento[i].remove();
	}
}


function teDescargoLaLocalidades(obj) {
	if (typeof obj === 'object') {
		// console.log(obj.localidades[3].provincia);
		var select = document.getElementById('provincias');
		var selectLocalidad = document.getElementById('localidades');
		var provinciaID = select.options[select.selectedIndex].value;
		for (var i = obj.localidades.length - 1; i >= 0; i--) {
			if (obj.localidades[i].provincia.id == provinciaID) {
				let nodoTexto = document.createTextNode(obj.localidades[i].nombre);
				let nodoElemento = document.createElement('option');
				nodoElemento.setAttribute('value', obj.localidades[i].id)
				nodoElemento.appendChild(nodoTexto);
				selectLocalidad.appendChild(nodoElemento);
			}

		}
	}else{
		return false;
	}
}



/*urlWithGet - idSelect*/
function cargarSelectPlaces() {
	let selectOrigen = document.getElementById('origen');
	let selectDestino = document.getElementById('destino');
	let url = '../Place.controller/place.controller.php?action=getAllPlace';
	let xmlInst = new XMLHttpRequest();
	xmlInst.open('GET', url , true)
	xmlInst.send(null);
	xmlInst.onreadystatechange = (evt)=>{
		if (evt.target.readyState == 4) {
			if (evt.target.status == 200) {
				let obj = JSON.parse(evt.target.responseText);
				for (var i = obj.length - 1; i >= 0; i--) {
					let nodeTextOrigen = document.createTextNode(obj[i].place_name);
					let nodoElementOrigen = document.createElement('option');
					nodoElementOrigen.setAttribute('value', obj[i].id)
					nodoElementOrigen.appendChild(nodeTextOrigen);
					selectOrigen.appendChild(nodoElementOrigen);

					let nodeTextDestino = document.createTextNode(obj[i].place_name);
					let nodoElementDestino = document.createElement('option');
					nodoElementDestino.setAttribute('value', obj[i].id)
					nodoElementDestino.appendChild(nodeTextDestino);
					selectDestino.appendChild(nodoElementDestino);
				}

			}
		}
	}
}

/*arrAtr = array[0=>campoNombreJson, ej.: place_name
				 1=>elementoACrear , ej.: element html <p>
			     2 ... =>Atributo a seterar, ej.: atributos del elemento a crear ej.: id, class, value,etc]
			     */
			     function loadSelect(urlP,idSelect,arrAtr) {
			     	let select = document.getElementById(idSelect);
			     	let url = urlP;
	// console.log(arrAtr[0]);
	// console.log(url);
	let xmlInst = new XMLHttpRequest();
	xmlInst.open('GET', url, true);
	xmlInst.send(null);
	xmlInst.onreadystatechange = (evt) =>{
		if (evt.target.readyState == 4) {
			if (evt.target.status == 200) {
				// console.log( `Result HTTP: ${evt.target.status} ${evt.target.statusText}`);
				let obj = JSON.parse(evt.target.responseText);
				// let obj = (evt.target.responseText);
				// console.log(obj);
				for (var i = obj.length - 1; i >= 0; i--) {
					// console.log(obj[i] + '.' + 'place_name');
					// console.log(obj[i][arrAtr[0]]);
					// console.log(typeof arrAtr[1]);
					let textNode = document.createTextNode(obj[i][arrAtr[0]]);
					let elementNode = document.createElement(arrAtr[1]);
					elementNode.setAttribute(arrAtr[2], obj[i].id);
					elementNode.appendChild(textNode);
					select.appendChild(elementNode);
				}

			}else{
				console.log( `Result HTTP: ${evt.target.status} ${evt.target.statusText}`);
			}
		}
	}
}


//url -> Direccionada a donde se encuentran los datos
/*Realizar esto mas reutilizable*/
function loadDynamicSelectChild(url,selectChild,evt) {
	limpiarSelect(selectChild);
	let textNode, elementNode;
	// /*Value - id_tabla de opcion seleccionada*/
	let id_select_parent = evt.target[evt.target.selectedIndex].value;
	let text_select_parent = evt.target[evt.target.selectedIndex].text;;
	let id_url = '&id='+ id_select_parent;	
	// console.log(url + '&id=' + id_select_parent);
	// console.log(url + id_url);
	// console.log(selectChild);
	xmlInst = new XMLHttpRequest();
	xmlInst.open('GET', url + id_url, true);
	xmlInst.send(null);
	xmlInst.onreadystatechange = (evt) =>{
		if (evt.target.readyState == 4) {
			if (evt.target.status == 200) {
				// console.log(`Result HTTP: ${evt.target.status} ${evt.target.statusText}`);
				let object = JSON.parse(evt.target.responseText);
				// console.log(object);
				if (object.length != 0) {
					for (var i = object.length - 1; i >= 0; i--) {
						object[i]
						textNode = document.createTextNode(object[i].id_d);
						elementNode = document.createElement('option');
						elementNode.setAttribute('value',object[i].id_d_int);
						elementNode.appendChild(textNode);
						selectChild.appendChild(elementNode);
					}
				}else{
					// alert('Sin servicio desde ' + text_select_parent );
					swal('Atencion!!!',`Sin servicio desde ${text_select_parent}`, 'info');
				}
				
			}else{
				console.log(`Result HTTP: ${evt.target.status} ${evt.target.statusText}`);
			}
		}

	}

}

