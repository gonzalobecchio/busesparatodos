<?php 
require_once(realpath($_SERVER['DOCUMENT_ROOT'] . '\\busesParaTodos\\dirs.php'));
include_once(realpath(CLASS_PATH .'bd.php'));
include_once(realpath(TRAITS . 'functions.trait.php'));
/**
 * 
 */
class ServicesDetails
{
	use functionsTrait;

	private $id_service;
	private $id_passanger;
	private $services_seat;
	private $services_date;
	public $db;
	private $table_name = "services_details";

	
	function __construct()
	{
		$this->db = new BD();
	}



	/*Setter*/

	public function setIdService($value)
	{
		$this->id_service = $value;
	}

	public function setIdPassanger($value)
	{
		$this->id_passanger = $value;
	}

	public function setServiceSeat($value)
	{
		$this->services_seat = $value;
	}

	public function setServicesDate($value)
	{
		$this->services_date = $value;
	}

	/*Getters*/

	public function getIdService()
	{
		return $this->id_service;
	}

	public function getIdPassanger()
	{
		return $this->id_passanger;
	}

	public function getServiceSeat()
	{
		return $this->services_seat;
	}

	public function getServicesDate()
	{
		return $this->services_date;
	}

	public function getTableName()
	{
		return $this->table_name;
	}


	/*Verifica si el objeto que ingresa es del tipo y si algun campo esta vacio o es null*/
	public function checkObject($value)
	{
		if (!is_object($value)) {
			return;
		}

		if (!isset($value->seat)) {
			throw new Exception("The date seat not exist or null");
			
		}

		if (!isset($value->item_services_id)) {
			throw new Exception("The date services_id not exist or null");
			
		}

		if (!isset($value->passenger_id)) {
			throw new Exception("The date passenger_id not exist or null");
			
		}

		if (!isset($value->date_search)) {
			throw new Exception("The date date_search not exist or null");
			
		}

		return $value;

	}

	/*Recibe un objeto y creal los setters*/
	public function createAllSetters($value)
	{
		if (!is_object($value)) {
			throw new Exception("value is not an object");
			
		}

		if (!($this->checkObject($value))) {
			throw new Exception("object does not exit or null");
			
		}
		

		if (!(is_numeric($value->seat) || is_int($value->seat))) {
			throw new Exception("value seat is not numeric or not integer");
			
		}

		if (!(is_numeric($value->item_services_id) || is_int($value->item_services_id))) {
			throw new Exception("value item_services_id is not numeric or not integer");
			
		}

		if (!(is_numeric($value->passenger_id) || is_int($value->passenger_id))) {
			throw new Exception("value passenger_id is not numeric or not integer");
			
		}

		if (!$this->is_date($value->date_search)) {
			throw new Exception("value date_search is not date");
			
		}

		$this->setServiceSeat($value->seat);
		$this->setServicesDate($value->date_search);
		$this->setIdPassanger($value->passenger_id);
		$this->setIdService($value->item_services_id);


	}	


	
	/*Metodo que recibe el objeto a insertar*/
	/*Return id registro insertado*/
	public function insertServiceDetails($values)
	{
		// echo ($values->seat);
		try {
			$this->createAllSetters($values);
			$this->db->query = "INSERT INTO {$this->getTableName()} (id_services, id_passenger, services_seat,services_date) VALUES ('{$this->getIdService()}', '{$this->getIdPassanger()}', '{$this->getServiceSeat()}', '{$this->getServicesDate()}')";
			return $this->db->ejecQuery($this->db->query);		
		} catch (Exception $e) {
			echo $e->getMessage();
		}	
	}


}